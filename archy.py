"""archy: A Python library for defining processor architectures"""

__author__ = "Christopher Tam"

from abc import abstractmethod, abstractclassmethod
from collections import defaultdict, OrderedDict
import enum
import functools
import inspect
import re
import struct
import sys
import typing
from typing import Dict, List, Set, Tuple

if sys.version_info[0] < 3 or (sys.version_info[0] == 3
                               and sys.version_info[1] < 6):
    raise Exception("Python version 3.6 or above is required")


def is_binary(val: str) -> bool:
    """Checks whether a provided string is a valid binary string.
    Valid binary strings are defined as one of the following two formats:
        (0|1)+
        0b(0|1)+
    
    Args:
        val (str): The value to check if binary.

    Returns:
        True if `val` is a valid binary string, False otherwise.
    """
    try:
        if int(val, base=2) >= 0:
            return True
    except:
        pass

    return False


def validatebinaryargs(bin_args: typing.Iterable[str]):
    """Ensures that all of the decorated function's arguments named in
    `bin_args` are valid binary strings. See `~archy.is_binary` for
    details on what is considered a valid binary string.
        
    Args:
        bin_args (Iterable[str]): Iterable of argument names to
            validate.
    
    Raises:
        ValueError: If an argument in `bin_args` is invalid.
        
    Examples:
        @validatebinaryargs(bin_args=['binary'])
        def myfunc(binary: str):
            value = int(binary, 2)
            ...
    """

    def decorator(func):
        sig = inspect.signature(func)

        for arg in bin_args:
            assert arg in sig.parameters

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            bound_args: inspect.BoundArguments = sig.bind(*args, **kwargs)
            bound_args.apply_defaults()

            for arg_name in bin_args:
                binary = bound_args.arguments[arg_name]

                if not isinstance(binary, str):
                    raise TypeError(f"Argument '{arg_name}' is not a string")
                if not is_binary(binary):
                    raise ValueError(
                        "Argument 'binary' is not a valid binary string")

            return func(*bound_args.args, **bound_args.kwargs)

        return wrapper

    return decorator


def prepend_binary_prefix(binary: str) -> str:
    """Appends the binary prefix '0b' to a binary string, if missing."""
    if binary.startswith('0b'):
        return binary

    return '0b' + binary


def prependbinaryprefixargs(bin_args: typing.Iterable[str]):
    """Prepends to all of the decorated function's arguments named in
    `bin_args` binary prefixes ('0b'). Uses
    `~archy.prepend_binary_prefix` internally.
        
    Args:
        bin_args (Iterable[str]): Iterable of argument names to
            prepend binary prefixes to.
    
    Examples:
        @prependbinaryprefixargs(bin_args=['binary'])
        def myfunc(binary: str):
            assert binary.startswith('0b')
            ...
    """

    def decorator(func):
        sig = inspect.signature(func)

        for arg in bin_args:
            assert arg in sig.parameters

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            bound_args: inspect.BoundArguments = sig.bind(*args, **kwargs)
            bound_args.apply_defaults()

            for arg_name in bin_args:
                binary = bound_args.arguments[arg_name]
                assert isinstance(binary, str)

                bound_args.arguments[arg_name] = strip_binary_prefix(binary)

            return func(*bound_args.args, **bound_args.kwargs)

        return wrapper

    return decorator


def strip_binary_prefix(binary: str) -> str:
    """Strips the binary prefix '0b' from a binary string, if present."""
    if binary.startswith('0b'):
        return binary[2:]

    return binary


def stripbinaryprefixargs(bin_args: typing.Iterable[str]):
    """Strips all of the decorated function's arguments named in
    `bin_args` of their binary prefixes ('0b'). Uses
    `~archy.strip_binary_prefix` internally.
        
    Args:
        bin_args (Iterable[str]): Iterable of argument names to
            strip of binary prefixes.
    
    Examples:
        @stripbinaryprefixargs(bin_args=['binary'])
        def myfunc(binary: str):
            assert not binary.startswith('0b')
            ...
    """

    def decorator(func):
        sig = inspect.signature(func)

        for arg in bin_args:
            assert arg in sig.parameters

        @functools.wraps(func)
        def wrapper(*args, **kwargs):
            bound_args: inspect.BoundArguments = sig.bind(*args, **kwargs)
            bound_args.apply_defaults()

            for arg_name in bin_args:
                binary = bound_args.arguments[arg_name]
                assert isinstance(binary, str)

                bound_args.arguments[arg_name] = strip_binary_prefix(binary)

            return func(*bound_args.args, **bound_args.kwargs)

        return wrapper

    return decorator


def is_hex(val: str) -> bool:
    """Checks whether a provided string is a valid hexadecimal string.
    
    Valid binary strings are defined as one of the following two formats:
        (0-9|A-F|a-f)+
        0x(0-9|A-F|a-f)+
    
    Args:
        val (str): The value to check if hexadecimal.

    Returns:
        True if `val` is a valid hexadecimal string, False otherwise.
    """
    try:
        if int(val, base=16) >= 0:
            return True
    except:
        pass

    return False


def prepend_hexadecimal_prefix(hexadecimal: str) -> str:
    """Appends the hexadecimal prefix '0x' to a hex string, if missing."""
    if hexadecimal.startswith('0x'):
        return hexadecimal

    return '0x' + hexadecimal


def strip_hexadecimal_prefix(hexadecimal: str) -> str:
    """Strips the hexadecimal prefix '0x' from a hex string, if present."""
    if hexadecimal.startswith('0x'):
        return hexadecimal[2:]

    return hexadecimal


@validatebinaryargs(bin_args=('binary', ))
@stripbinaryprefixargs(bin_args=('binary', ))
def sign_extend(binary: str, target_width: int) -> str:
    """Sign extends a binary string.
    
    Args:
        binary (str): The binary string to sign extend.
        target_width (int): The bit width to sign extend to.

    Examples:
        >>> sign_extend('0b1101', 8)
        11111101

        >>> sign_extend('0101', 8)
        00000101
    """
    if target_width <= 0:
        raise ValueError("Invalid target_width for sign extension")

    if len(binary) >= target_width:
        return binary

    return binary.rjust(target_width, binary[0])


@validatebinaryargs(bin_args=('binary', ))
@stripbinaryprefixargs(bin_args=('binary', ))
def zero_extend(binary: str, target_width: int, pad_right: bool = False):
    """Zero extends a binary string."""
    if pad_right:
        return binary.ljust(target_width, '0')
    else:
        return binary.rjust(target_width, '0')


@validatebinaryargs(bin_args=('binary', ))
def bin2hex(binary: str) -> str:
    """Converts a binary string to a hexadecimal string."""
    return hex(int(binary, 2))


def hex2bin(hexadecimal: str) -> str:
    """Converts a hexadecimal string to a binary string."""
    return bin(int(hexadecimal, 16))


@validatebinaryargs(bin_args=('binary', ))
@stripbinaryprefixargs(bin_args=('binary', ))
def bin2dec(binary: str, bit_width: int) -> int:
    """Computes the 2's complement Python value of a binary string.
    
    Keyword arguments:
    binary -- the binary string to convert
    bit_width -- the bit width with which to interpret the binary string"""
    if len(binary) > bit_width:
        raise ValueError(f"Length of binary string exceeds bit width")

    if (binary & (1 << (bit_width - 1))) != 0:
        binary = binary - (1 << bit_width)
    return binary


def dec2bin(val: int, bit_width: int) -> str:
    """Computes the 2's complement binary string of an int value.
    
    Keyword arguments:
    val -- the Python int to convert
    bit_width -- the maximum width of the resulting binary string
    """
    return format(val if val >= 0 else (1 << bit_width) + val,
                  f'0{bit_width}b')


class Endianness(enum.Enum):
    """The endianness of a binary value."""
    BIG = enum.auto()
    LITTLE = enum.auto()


class Value:
    """Represents a set of bits interpretable as a value.
    """

    @classmethod
    def from_single(cls, val: float, endian: Endianness):
        """Creates a Value from a single-precision floating-point
        interpretation of a Python floating-point value.
        
        Args:
            val (float): The Python floating-point to convert from.
            endian (endian): The endianness to store the Value in.
        """
        if endian == Endianness.LITTLE:
            b: bytes = struct.pack("<f", val)
        else:
            b: bytes = struct.pack(">f", val)

        # Unpack as big endian since endian conversion was already made
        binary: str = bin(int.from_bytes(b, 'big'))

        # Zero extend binary since int.from_bytes() ignores most
        # significant 0s
        binary = zero_extend(binary, 32)

        return cls(binary)

    @classmethod
    def from_double(cls, val: float, endian: Endianness):
        """Creates a Value from a double-precision floating-point
        interpretation of a Python floating-point value.
        
        Args:
            val (float): The Python floating-point to convert from.
            endian (endian): The endianness to store the Value in.
        """
        if endian == Endianness.LITTLE:
            b: bytes = struct.pack("<d", val)
        else:
            b: bytes = struct.pack(">d", val)

        # Unpack as big endian since endian conversion was already made
        binary: str = bin(int.from_bytes(b, 'big'))

        # Zero extend binary since int.from_bytes() ignores most
        # significant 0s
        binary = zero_extend(binary, 64)

        return cls(binary)

    @classmethod
    def from_int(cls, val: int, endian: Endianness, bit_width: int = 0):
        """Creates a Value from a Python integer value.
        
        Args:
            val (float): The Python integer to convert from.
            endian (endian): The endianness to store the Value in.
            bit_width (:obj:`int`, optional): The number of bits to make
                the Value. If `bit_width` <= 0, the value will use the
                minimum number of bits needed to represent `val`.
                Defaults to 0.

        Raises:
            OverflowError: If `bit_width` is greater than 0 but less
                than the number of bits required to represent `val`.
        """
        if val == 0:
            num_bits = 1
        elif val < 0:
            # Number of bits to represent complement + 1 bit for sign
            num_bits = (~val).bit_length() + 1
        else:
            num_bits = val.bit_length()

        if bit_width > 0 and bit_width < num_bits:
            raise OverflowError(
                "bit_width must be >= the number of bits needed to represent "
                "val")

        num_bytes = (num_bits // 8) + min(num_bits % 8, 1)

        b: bytes = val.to_bytes(num_bytes, endian.name.lower(), signed=True)

        # Unpack as big endian since endian conversion was already made
        binary: str = bin(int.from_bytes(b, 'big'))[-num_bits:]

        val = cls(binary)
        if val.bit_width < bit_width:
            val = val.sign_extend(bit_width)

        return val

    @validatebinaryargs(bin_args=('binary', ))
    @stripbinaryprefixargs(bin_args=('binary', ))
    def __init__(self, binary: str):
        self._binary = binary

    @property
    def binary(self) -> str:
        """The binary string of the value being represented."""
        return self._binary

    @property
    def hex(self) -> str:
        """The hex string of the value being represented.
        
        If the bit width of the binary is not a multiple of 4, the
        left-most hexadecimal digit will assume left 0 padding.
        """
        return hex(int(self.binary, base=2))

    @property
    def bit_width(self) -> int:
        """The bit width of the value being represented."""
        return len(self.binary)

    def bytes(self, endian: Endianness):
        """A generator function that iterates over the bytes.
        
        Note that if the value is not byte-aligned, the most significant
        "byte" will not be a full 8 bits.

        Args:
            endian (Endianness): The endianness with which to interpret
                the value.

        Yields:
            str: The next byte.
        """

        num_non_aligned = self.bit_width % 8

        if endian == Endianness.LITTLE:
            start = self.bit_width - num_non_aligned - 8
            stop = -1
            step = -8
            if num_non_aligned > 0:
                yield self.binary[self.bit_width - num_non_aligned:]
        else:
            start = num_non_aligned
            stop = self.bit_width
            step = 8
            if num_non_aligned > 0:
                yield self.binary[:num_non_aligned]

        for i in range(start, stop, step):
            yield self.binary[i:i + 8]

    def is_byte_aligned(self) -> bool:
        return self.bit_width % 8 == 0

    def sign_bit(self, endian: Endianness) -> str:
        """Gets the sign bit of the value."""
        return next(self.bytes(endian))[0]

    def flip_endianness(self):
        """Flips the byte ordering of the value.
        
        Returns:
            A new Value containing the binary of opposite endianness.

        Raises:
            AttributeError: If the current value is not byte aligned.
        """
        binary = ''.join(self.bytes(Endianness.LITTLE))
        return Value(binary)

    def sign_extend(self, target_width: int, endian: Endianness):
        """Sign-extends the value to a target bit width.

        Args:
            target_width (int): The new target bit width.
            endian (Endianness): The Endianness with which to interpret
                the current value as.

        Returns:
            The new sign-extended Value.

        Raises:
            ValueError: If target_width is less than the current
                Value.bit_width.
        """
        if target_width < self.bit_width:
            raise ValueError("Target width is less than bit width of value")
        elif target_width == self.bit_width:
            return self

        if endian == Endianness.LITTLE:
            # This may not be the most efficient but logic is simpler
            return self.flip_endianness().sign_extend(
                target_width, Endianness.BIG).flip_endianness()
        elif endian == Endianness.BIG:
            binary = self.binary.rjust(target_width, self.binary[0])
            return Value(binary)

    def zero_extend(self, target_width: int, endian: Endianness):
        """Zero-extends the value to a target bit width.

        Args:
            target_width (int): The new target bit width.
            endian (Endianness): The Endianness with which to interpret
                the current value as.

        Returns:
            The new zero-extended Value.

        Raises:
            ValueError: If target_width is less than the current
                Value.bit_width.
        """
        if target_width < self.bit_width:
            raise ValueError("Target width is less than bit width of value")
        elif target_width == self.bit_width:
            return self

        if endian == Endianness.LITTLE:
            # This may not be the most efficient but logic is simpler
            return self.flip_endianness().sign_extend(
                target_width, Endianness.BIG).flip_endianness()
        elif endian == Endianness.BIG:
            binary = self.binary.rjust(target_width, 0)
            return Value(binary)

    def one_extend(self, target_width: int, endian: Endianness):
        """Zero-extends the value to a target bit width.

        Args:
            target_width (int): The new target bit width.
            endian (Endianness): The Endianness with which to interpret
                the current value as.

        Returns:
            The new zero-extended Value.

        Raises:
            ValueError: If target_width is less than the current
                Value.bit_width.
        """
        if target_width < self.bit_width:
            raise ValueError("Target width is less than bit width of value")
        elif target_width == self.bit_width:
            return self

        if endian == Endianness.LITTLE:
            # This may not be the most efficient but logic is simpler
            return self.flip_endianness().sign_extend(
                target_width, Endianness.BIG).flip_endianness()
        elif endian == Endianness.BIG:
            binary = self.binary.rjust(target_width, 0)
            return Value(binary)

    def to_signed_int(self, endian: Endianness) -> int:
        """Interprets the current value as a signed 2's complement
        integer.
        
        Args:
            endian (Endianness): The Endianness with which to interpret
                the current value as.
        """
        if self.bit_width == 1:
            return int(self.binary, base=2)
        elif endian == Endianness.LITTLE:
            return self.flip_endianness().to_signed_int(Endianness.BIG)

        positive = int(self.binary[1:], base=2)
        if self.binary[0] == '0':
            return positive
        else:
            return -positive - 1

    def to_unsigned_int(self, endian: Endianness) -> int:
        """Interprets the current value as an unsigned integer.
        
        Args:
            endian (Endianness): The Endianness with which to interpret
                the current value as.
        """
        if endian == Endianness.LITTLE:
            return self.flip_endianness().to_unsigned_int(Endianness.BIG)
        return int(self.binary, base=2)

    def __bool__(self) -> bool:
        for bit in self.binary:
            if bit == '1':
                return True
        return False

    def __len__(self) -> int:
        return self.bit_width

    def __invert__(self):
        res = ''.join('1' if bit == '0' else '0' for bit in self.binary)
        return Value(res)

    def __or__(self, other):
        self._check_bitwise_other(other)
        res = ''.join('1' if a == '1' or b == '1' else '0'
                      for a, b in zip(self.binary, other.binary))
        return Value(res)

    def __and__(self, other):
        self._check_bitwise_other(other)
        res = ''.join('0' if a == '0' or b == '0' else '1'
                      for a, b in zip(self.binary, other.binary))
        return Value(res)

    def __repr__(self) -> str:
        return prepend_binary_prefix(self.binary)

    def __str__(self) -> str:
        return self.binary

    def __hash__(self) -> int:
        return hash(self.binary)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Value):
            return False

        return self.binary == other.binary

    def _check_bitwise_other(self, other):
        """Internal function to check whether a bitwise operation can 
        be performed with another object."""
        if not isinstance(other, Value):
            raise TypeError(
                "Cannot perform bitwise operation with non-Value type")
        elif self.bit_width != other.bit_width:
            raise ValueError(
                "Cannot perform bitwise operation on Value type with "
                "different bit width")


class Register:
    """Represents a single register."""

    def __init__(self, address: int, name: str, value: Value,
                 identifiers: Set[str]):
        """Constructs an instance of a register.

        Args:
            address (int): The address of the register. If this value is
                negative, this indicates the register is not an
                addressable register.
            name (str): The human readable name of the register.
            value (Value): The Value to initialize the register to.
            identifiers (Set[str]): The set of different names the
                register can be referred to in code.
        """
        self._address = address
        self._name = name
        self._value = value
        self._identifiers = identifiers

    @property
    def address(self) -> int:
        """The address of the register.
        
        If this value is negative, this indicates the register is not
        an addressable register."""
        return self._address

    @property
    def name(self) -> str:
        """The human readable name of the register."""
        return self._name

    @property
    def identifiers(self) -> Set[str]:
        """The set of different names the register can be referred to in
        code."""
        return self._identifiers

    @property
    def value(self) -> Value:
        """The value the register is currently holding."""
        return self._value

    @value.setter
    def value(self, value: Value):
        if value.bit_width != self._value.bit_width:
            raise ValueError(
                f"Changing the bit width of register {self._name}")

        self._value = value

    @property
    def bit_width(self) -> int:
        """The width of the register, in bits."""
        return self._value.bit_width

    def __str__(self) -> str:
        return self.name

    def __repr__(self) -> str:
        return f"{self.name}: {self.value.binary}"

    def __hash__(self) -> int:
        return hash(self.name)

    def __eq__(self, other) -> bool:
        if not isinstance(other, Register):
            return False
        return self.name == other.name


class RegisterSet:
    """A set of registers in an architecture."""

    def __init__(self, registers: typing.Iterable[Register]):
        self._register_map = OrderedDict()
        self._identifier_map: Dict[str, Register] = {}
        self._address_map: Dict[str, Register] = {}

        for reg in registers:
            # Add to name -> Register map
            if reg.name in self._register_map:
                raise ValueError(f"Register {reg.name} is defined twice")

            self._register_map[reg.name] = reg

            # Add to identifiers -> Register map
            for identifier in reg.identifiers:
                if identifier in self._identifier_map:
                    raise ValueError(
                        f"Register code name {identifier} is defined twice")
                self._identifier_map[identifier] = reg

            # Add to address -> Register map
            if reg.address >= 0:
                if reg.address in self._address_map:
                    conflict_reg = self._address_map[reg.address]
                    raise ValueError(
                        f"Addresses of {reg.name} and {conflict_reg.address} "
                        "conflict")
                self._address_map[reg.address] = reg

    @property
    def registers(self) -> Tuple[Register]:
        """A set-like view of all of the registers."""
        return self._register_map.items()

    def get_register_by_address(self, num: int) -> Register:
        """Gets a register by its address.
        
        Raises:
            ValueError: If the register address is negative.
            LookupError: If the register address does not exist.
        """
        if num < 0:
            raise ValueError("Register address cannot be negative")

        try:
            return self._registers_map[num]
        except IndexError as e:
            raise LookupError(
                f"Register number {num} does not exist in the register set"
            ) from e

    def get_register_by_identifier(self, identifier: str) -> Register:
        """Gets a register by an identifier.

        Raises:
            ValueError: If the register address is negative.
            LookupError: If the identifier does not refer to any register.
        """
        try:
            return self._identifier_map[identifier]
        except IndexError as e:
            raise LookupError(
                f"'{identifier}' is not a valid register identifier") from e

    def get_register_by_name(self, name: str) -> Register:
        """Gets a register by its name."""
        try:
            return self._register_map[name]
        except KeyError as e:
            raise LookupError(
                f"Register {name} does not exist in the register set") from e

    def __getitem__(self, key) -> Register:
        if isinstance(key, int):
            return self.get_register_by_number(key)
        elif isinstance(key, str):
            return self.get_register_by_name(key)

        raise ValueError(f"Cannot index register set by type {type(key)}")

    def __setitem__(self, key, value: Value):
        reg: Register = self[key]
        reg.value = value


class AbstractInstruction:
    """This is the base class that all implementations of instructions
    must override."""

    __RE_NO_OPERANDS = re.compile(r'^\s*$')

    @classmethod
    def opcode(cls) -> int:
        """Return the operation code for the given instruction as an
        integer."""
        raise NotImplementedError()

    def __init__(self, operands, pc, instruction):
        self.__operands = operands
        self.bin_operands = self.parse_operands(operands, pc, instruction)
        self.__pc = pc
        self.__instruction = instruction

    @classmethod
    def create(cls, operands, pc, instruction) -> list:
        """Generates a list of Instruction(s) for the given operands."""
        raise NotImplementedError()

    @classmethod
    def pc(cls, pc: int, **kwargs) -> int:
        """Return the new PC after assembling the given instruction"""
        # By default, return pc + 1
        return pc + 1

    @classmethod
    def parse_operands(cls, operands, pc, instruction) -> str:
        match = cls.__RE_NO_OPERANDS.match(operands)

        if match is None:
            raise RuntimeError(
                f"Operands '{operands.strip()}' are not permitted.")

        return ''

    @abstractmethod
    def binary(self):
        """Assemble the instruction into binary form.
        
        Returns:
            (str): A binary string representing the instruction.
        """
        raise NotImplementedError()

    def hex(self):
        """Assemble the instruction into binary form.
        
        Returns:
            (str): A hexadecimal string representing the instruction.
        """
        return bin2hex(self.binary())


class AbstractInstructionSet:
    """A set of instructions, typically associated with an architecture."""

    def __init__(self, instruction_classes: typing.Iterable[type]):
        self._opcode_map = OrderedDict()

        for instr in instruction_classes:
            if not issubclass(instr, AbstractInstruction):
                raise TypeError(f"{instr} is not a valid instruction type")

            if instr.opcode in self._opcode_map:
                raise ValueError(
                    f"Opcodes of {self._opcode_map[instr.opcode.__name__]} "
                    f"{instr.__name__} conflict")
            self._opcode_map[instr.opcode] = instr

    @property
    def instructions(self):
        """A set-like view of all of the instructions."""
        return self._opcode_map.items()

    def get_instruction_by_opcode(self, opcode: int) -> Register:
        """Gets an instruction by its opcode."""
        try:
            return self._opcode_map[opcode]
        except IndexError as e:
            raise LookupError(
                f"Opcode {opcode} does not map to an instruction") from e

    def __getitem__(self, key) -> Register:
        if isinstance(key, int):
            return self.get_instruction_by_opcode(key)

        raise ValueError(f"Cannot index instruction set by type {type(key)}")


class AbstractArchitecture:
    @property
    @abstractmethod
    def name(self) -> str:
        raise NotImplementedError()

    @property
    @abstractmethod
    def endianness(self) -> Endianness:
        raise NotImplementedError()

    @property
    @abstractmethod
    def bit_width(self) -> int:
        """Returns the bit width of the architecture"""
        raise NotImplementedError()

    @property
    @abstractmethod
    def opcode_width(self) -> int:
        """Returns the (first-level) opcode width, in bits"""
        raise NotImplementedError()

    @property
    @abstractmethod
    def register_address_width(self) -> int:
        """The number of bits used to identify registers"""
        raise NotImplementedError()

    @property
    @abstractmethod
    def register_set(self) -> RegisterSet:
        raise NotImplementedError()

    @abstractmethod
    def get_instructions(self):
        """Returns a dictionary that maps opcodes to an Instruction
        type"""
        raise NotImplementedError()

    @abstractmethod
    def get_instruction_by_name(
            self, instr_name: str) -> typing.Type[AbstractInstruction]:
        raise NotImplementedError()

    @abstractmethod
    def get_instruction_by_opcode(
            self, opcode: str) -> typing.Type[AbstractInstruction]:
        raise NotImplementedError()
